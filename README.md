# Landing page

Responsive landing page inspired by MegaOne (https://megaone.acrothemes.com/index-agency.html)

### Live preview

- [Live page](https://creative-sln.netlify.app/)

### Development stack

- Webpack (originally developed with webpack configuration)
- Vite (migrated from webpack)
- HTML, SCSS, Javascript
- Netlify deployment

### Build

- Install node modules `npm install`
- Run `npm run dev` to build landing page in development mode. Use this if you want to develop new features and see changes in real time in browser
- Run `npm run build` to build landing page in production mode.
- Run `npm run preview` View built website in browser (execute `npm run build` first).
