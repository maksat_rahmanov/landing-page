import "./styles/styles.scss";

document.addEventListener("DOMContentLoaded", () => {
  onIntersectionObserver();
  onMenuToggle();
  initTestimonials();
  initDate();
});

function onIntersectionObserver() {
  const header = document.querySelector(".header");
  const hero = document.querySelector(".hero");
  const options = { rootMargin: "-100px 0px 0px 0px" };
  const sticky = "sticky";

  const observer = new IntersectionObserver((entries, observer) => {
    entries.forEach((entry) => {
      if (!entry.isIntersecting) {
        header.classList.add(sticky);
      } else {
        header.classList.remove(sticky);
      }
    });
  }, options);

  observer.observe(hero);
}

function onMenuToggle() {
  let isMenuOpen = false;
  const openModifier = "menu-open";
  const toggleMenu = document.querySelector("#menu-toggle");
  const navBar = document.querySelector("#nav-bar");

  toggleMenu.addEventListener("click", () => toggle());

  navBar.addEventListener("click", (e) => {
    if (e.target.tagName === "A" && isMenuOpen) {
      toggle();
    }
  });

  function toggle() {
    isMenuOpen = !isMenuOpen;
    navBar.classList.toggle(openModifier);
    toggleMenu.classList.toggle(openModifier);
    document.body.style.overflow = `${isMenuOpen ? "hidden" : "unset"}`;
  }
}

function initTestimonials() {
  const testimonials = document.querySelector("#testimonials");
  const interval = 3500; // 3.5 seconds

  setInterval(() => {
    testimonials.style.transform = "translate(-33%)";
  }, interval);

  testimonials.addEventListener("transitionend", () => {
    testimonials.appendChild(testimonials.firstElementChild);
    testimonials.style.transition = "none";
    testimonials.style.transform = "translate(0)";

    setTimeout(() => {
      testimonials.style.transition = "all 900ms";
    });
  });
}

function initDate() {
  const year = document.querySelector("#currentYear");
  year.innerText = new Date().getFullYear();
}
